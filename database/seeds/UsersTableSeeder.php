<?php

use Illuminate\Database\Seeder;
use App\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = User::create([
            'firstname'                 => 'Admin',
            'middlename'                => '',
            'lastname'                  => 'Inistrator',
            'email'                     => 'admin@myeg.ph',
            'is_active'                 => '1',
            'email_verified_at'         => \Carbon\Carbon::now(),
            'password_change_at'        => \Carbon\Carbon::now(),
            'password'                  => bcrypt('Password!123'),
        ]);

        $user->assignRole('super admin');
    }
}
