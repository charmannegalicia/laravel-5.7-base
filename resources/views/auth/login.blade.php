@extends('layouts.app')

@section('content')
<div class="container">
    @if(session()->has('failed'))
        <div class="row">
            <div class="col-md-12">
                <div class="alert alert-danger fade in">
                    <strong>Error!</strong> {{ session('failed') }}
                </div>
            </div>
        </div>
    @endif
        <center>
        {{-- <div class="login-container custom-border2">
            <div class="custom-login-header custom-border1">
                    <img class="nav-lbp-logo"
                src="{{ asset('images/main-logo.png') }}"
                alt="LBP | MYEG"
                itemprop="logo"/>
            </div>
            <div class="custom-card">
                    <form method="POST" action="{{ route('login') }}" class="custom-form">
                        @csrf
                        <center>
                        <div class="form-group row" style="width: 250px; margin-bottom: 0;">
                            <div class="form-group">
                                <div class="input-group">
                                    <div class="input-group-addon" style="background: #ffc42d;color: white;border-color: #ffc500;border-left-width: 3px;border-top-width: 3px;border-bottom-width: 3px;">
                                        <span class="glyphicon glyphicon-user" aria-hidden="true"></span>
                                    </div>
                                    <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required autofocus placeholder="Email Address">
                                    
                                </div>
                                <div style="margin-top: 10px;">
                                @if ($errors->has('email'))
                                    <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                                </div>
                            </div>
                        </div>
                        <div class="form-group row" style="width: 250px; margin-bottom: 0;">
                            <div class="form-group" style="margin-bottom: 0;">
                                <div class="input-group">
                                    <div class="input-group-addon" style="background: #ffc42d;color: white;border-color: #ffc500;border-left-width: 3px;border-top-width: 3px;border-bottom-width: 3px;">
                                        <span class="glyphicon glyphicon-lock" aria-hidden="true"></span>   
                                    </div>
                                    <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required placeholder="Password">
                                </div>
                                <div style="margin-top: 10px;">
                                    @if ($errors->has('password'))
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </center>

                        <div class="form-group row" style="padding-top: 10px;padding-left: 10px;">
                            <div class="col-xs-12">
                                <div class="form-check">
                                    <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>

                                    <label class="form-check-label label-left" for="remember">
                                        {{ __('Remember Me') }}
                                    </label>
                                </div>
                            </div>
                            
                        </div>
                        <div class="form-group row" style="margin-bottom: 0px;">
                            <div class="col-md-12">
                                <button type="submit" class="btn custom-button">
                                    {{ __('Login') }}
                                </button>
                            </div>
                        </div>
                        <div class="form-group row" style="margin-bottom: 0px; padding-top: 10px">
                            <div class="col-xs-12">
                                    <div>
                                        <a class="btn btn-link" href="{{ route('password.request') }}" style="padding: 0; color:white;">
                                            {{ __('Forgot Your Password?') }}
                                        </a>
                                    </div>
                                </div>
                        </div>
                        <div>
                            <span>
                                Don’t have an account?
                            </span>
                            <a href="{{ route('register')}}">
                                Sign up now
                            </a>
                            @if (Route::has('password.request'))
                                
                            @endif
                        </div>
                    </form>
            </div>
        </div> --}}
        <br>
        <div class="row">
            <div class="col-md-4"></div>
            <div class="col-md-4 custom-login-form-container">
                <div class="">
                        <img class="nav-lbp-logo"
                    src="{{ asset('images/main-logo.png') }}"
                    alt="LBP | MYEG"
                    itemprop="logo"/>
                </div>
                <div class="custom-card">
                        <form method="POST" action="{{ route('login') }}" class="custom-form">
                            @csrf
                            <center>
                            <div class="form-group row" style="margin-bottom: 0;">
                                <div class="form-group">
                                    <div class="input-group">
                                        <div class="input-group-addon" style="background: #ffc42d;color: white;border-color: #ffc500;border-left-width: 3px;border-top-width: 3px;border-bottom-width: 3px;">
                                            <span class="glyphicon glyphicon-user" aria-hidden="true"></span>
                                        </div>
                                        <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required autofocus placeholder="Email Address">
                                    </div>
                                    <div style="margin-top: 10px;">
                                        @if ($errors->has('email'))
                                            <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('email') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row" style=" margin-bottom: 0;">
                                <div class="form-group" style="margin-bottom: 0;">
                                    <div class="input-group">
                                        <div class="input-group-addon" style="background: #ffc42d;color: white;border-color: #ffc500;border-left-width: 3px;border-top-width: 3px;border-bottom-width: 3px;">
                                            <span class="glyphicon glyphicon-lock" aria-hidden="true"></span>   
                                        </div>
                                        <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required placeholder="Password">
                                    </div>
                                    <div style="margin-top: 10px;">
                                        @if ($errors->has('password'))
                                            <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('password') }}</strong>
                                        </span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </center>
    
                            <div class="form-group row">
                                <div class="col-md-12">
                                    <div class="form-check">
                                        <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>
    
                                        <label class="form-check-label label-left" for="remember">
                                            {{ __('Remember Me') }}
                                        </label>
                                    </div>
                                </div>
                                
                            </div>
                            <div class="form-group row" style="margin-bottom: 0px;">
                                <div class="col-md-12">
                                    <button type="submit" class="btn custom-button">
                                        {{ __('Login') }}
                                    </button>
                                </div>
                            </div>
                            <div class="form-group row" style="margin-bottom: 0px; padding-top: 10px">
                                <div class="col-xs-12">
                                        <div>
                                            <a class="btn btn-link" href="{{ route('password.request') }}" style="padding: 0; color:white;">
                                                {{ __('Forgot Your Password?') }}
                                            </a>
                                        </div>
                                    </div>
                            </div>
                            <div>
                                <span>
                                    Don’t have an account?
                                </span>
                                <a href="{{ route('register')}}">
                                    Sign up now
                                </a>
                                @if (Route::has('password.request'))
                                    
                                @endif
                            </div>
                        
                </div>
            </div>
            <div class="col-md-4"></div>
        </div>
        <div class="col-md-4"></div>
        <div class="col-md-4">
            <div class="form-group">
                <a href="{{ url('/auth/facebook') }}" class="btn btn-facebook"><i class="fa fa-facebook"></i> Facebook</a>
                <a href="{{ url('/auth/google') }}" class="btn btn-google"><i class="fa fa-google"></i> Google</a>
            </div>
        </div>
        <div class="col-md-4"></div>
        </div>
        
    </form>
        </center>
</div>
@endsection
