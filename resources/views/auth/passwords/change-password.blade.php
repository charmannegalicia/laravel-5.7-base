@extends('layouts.app')

@section('content')
<div class="container">
    <center>
        <div class="row">
        <div class="col-md-4"></div>
        <div class="col-md-4 custom-login-form-container">
                <div class="custom-header custom-border1">Change Password</div>
                <div class="custom-card">

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    <form method="POST" action="{{ route('user.password.update') }}">
                        @csrf
                        @method('patch')
                        <div class="form-group row">
                            <label for="password" class="col-md-5 col-form-label text-md-right">Current Password</label>

                            <div class="col-md-7">
                                <input id="password" type="password" class="form-control {{ session('invalid_current_pass') ? 'has-error' : '' }}" name="current_password" required>

                                @if (session('invalid_current_pass'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ session('invalid_current_pass') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="password" class="col-md-5 col-form-label text-md-right">{{ __('Password') }}</label>

                            <div class="col-md-7">
                                <input id="password" type="password" class="form-control {{ $errors->has('password') ? 'has-error' : '' }}" name="password" required>

                                @if ($errors->has('password'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password-confirm" class="col-md-5 col-form-label text-md-right">{{ __('Confirm Password') }}</label>

                            <div class="col-md-7">
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-12 text-right">
                                <button type="submit" class="btn custom-button">
                                    Submit
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="col-md-4"></div>
        </div>
    </center>
</div>
@endsection