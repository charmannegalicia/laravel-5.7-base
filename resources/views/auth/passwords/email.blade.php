@extends('layouts.app')

@section('content')
<div class="container">
    <center>
        <div class="row">
        <div class="col-md-4"></div>
        <div class="col-md-4 custom-login-form-container">
                <div class="custom-header custom-border1">Reset Password</div>
                <div class="custom-card">

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    <form method="POST" action="{{ route('password.email') }}">
                        @csrf
                        <center>
                        <div class="form-group row">
                            <div class="custom-250-width">
                                <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required placeholder="Email Address">

                                @if ($errors->has('email'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="custom-250-width">
                                <button type="submit" class="btn custom-button">
                                    {{ __('Send Password Reset Link') }}
                                </button>
                            </div>
                        </div>
                    </center>
                    </form>
                </div>
            </div>
        </div>
        <div class="col-md-4"></div>
    </div>
    </center>
</div>
@endsection

{{-- <style>
.login-container{
    margin-top: 50px;
    width: 450px;
    box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);
    text-align: center;
}
.form-label{
    text-align: right;
}
.custom-button{
    background-color: #d32123;
    border-color: #d32123;
    color: white;
}
.custom-card{
    padding: 40px 40px;
}
.custom-header{
    background: #d51e17;
    color: white;
    font-size: 30px;
    padding: 30px 0;
}
.custom-border1{
    border-radius: 20px 20px 0px 0px;
}
.custom-border2{
    border-radius: 20px;
}
.form-check{
    text-align: center;
}
form{
    margin-bottom: 0;
}
.invalid-feedback{
    color:#d71b03
}
</style> --}}
