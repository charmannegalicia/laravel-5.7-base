@extends('layouts.app')

@section('content')
<div class="container">
        <center>
        <div class="row">
        <div class="col-md-4"></div>
        <div class="col-md-4 custom-login-form-container">
            <div class="custom-header custom-border1">Register</div>
            <div class="custom-card">
                <div class="card-body">
                    <form method="POST" action="{{ route('register') }}">
                        @csrf

                        <div class="form-group row">
                            <label for="firstname" class="col-md-4 form-label">Firstname*</label>

                            <div class="col-md-8">
                                <input id="firstname" type="text" class="form-control{{ $errors->has('firstname') ? ' is-invalid' : '' }}" name="firstname" value="{{ old('firstname') }}" required autofocus>

                                @if ($errors->has('firstname'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('firstname') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group row">
                                <label for="middlename" class="col-md-4 form-label">Middlename</label>
    
                                <div class="col-md-8">
                                    <input id="middlename" type="text" class="form-control{{ $errors->has('middlename') ? ' is-invalid' : '' }}" name="middlename" value="{{ old('middlename') }}" required autofocus>
    
                                    @if ($errors->has('middlename'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('middlename') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group row">
                                    <label for="lastname" class="col-md-4 form-label">Lastname</label>
        
                                    <div class="col-md-8">
                                        <input id="lastname" type="text" class="form-control{{ $errors->has('lastname') ? ' is-invalid' : '' }}" name="lastname" value="{{ old('lastname') }}" required autofocus>
        
                                        @if ($errors->has('lastname'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('lastname') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>

                        <div class="form-group row">
                            <label for="email" class="col-md-4 form-label">{{ __('E-Mail Address') }}</label>

                            <div class="col-md-8">
                                <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required>

                                @if ($errors->has('email'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password" class="col-md-4 form-label">{{ __('Password') }}</label>

                            <div class="col-md-8">
                                <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>

                                @if ($errors->has('password'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password-confirm" class="col-md-4 form-label">{{ __('Confirm Password') }}</label>

                            <div class="col-md-8">
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-12">
                                <button type="submit" class="btn custom-button">
                                    {{ __('Register') }}
                                </button>
                            </div>
                        </div>
                        <br>
                        <div>
                            <span>
                                Already have an account?
                            </span>
                            <a href="{{ route('login')}}">
                                Sign in
                            </a>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="col-md-4"></div>
        </div>
        </center>
</div>
@endsection

{{-- <style>
        .login-container{
           margin-top: 50px;
           width: 450px;
           box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);
           text-align: center;
        }
        .form-label{
            text-align: right;
        }
        .custom-button{
           background-color: #d32123;
           border-color: #d32123;
           color: white;
        }
        .custom-card{
           padding: 40px 40px;
        }
        .custom-header{
           background: #d51e17;
           color: white;
           font-size: 30px;
           padding: 30px 0;
        }
        .custom-border1{
           border-radius: 20px 20px 0px 0px;
        }
        .custom-border2{
           border-radius: 20px;
        }
        .form-check{
            text-align: right;
        }
        form{
            margin-bottom: 0;
        }
       </style> --}}