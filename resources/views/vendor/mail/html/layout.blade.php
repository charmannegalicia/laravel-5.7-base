<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
</head>
<body>
    <style>
        @media only screen and (max-width: 600px) {
            .inner-body {
                width: 100% !important;
            }

            .footer {
                width: 100% !important;
            }
        }

        @media only screen and (max-width: 500px) {
            .button {
                width: 100% !important;
            }
        }

        .email-body{
            margin-left: 20px;
            margin-right: 20px; 
            background: white;
            padding: 20px;
        }
        .email-logo{
            height: 50px;
        }
        .custom-email-header{
            margin-left: 20px;
            margin-right: 20px;
            margin-top: 20px;
            padding: 10px;
            background: #c61e1f;
        }
        .custom-footer{
            background: #353535;
            padding: 20px;
            margin: 0 20px;
            margin-bottom: 20px;
        }
    </style>
    {{-- {{ $header ?? '' }} --}}
    {{-- {{ $subcopy ?? '' }}
    {{ $footer ?? '' }} --}}

    <div class="custom-email-header">
    <center><img src="{{asset('images/main-logo.png')}}" class="email-logo"></center>

    </div>
    
    <div class="email-body">
    {{ Illuminate\Mail\Markdown::parse($slot) }}
    </div>
    
    <footer class="custom-footer">
        <center>MYEG</center>
    </footer>



</body>
</html>
