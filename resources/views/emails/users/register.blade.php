

<div class="custom-email-header">
<center><img src="{{asset('images/main-logo.png')}}" class="email-logo"></center>

</div>

<div class="email-body">
    <p>Hi {{ $name }},</p>
    <p class="custom-indent">Your temporary password is: <strong>{{ $password }}</strong></p>
    <p class="custom-indent">Please login here: <a href="{{ url('/') }}/login">{{ url('/')}}/login</a></p>
    <p>Thank You.</p>
    <p><strong>Your MYEG Team</strong></p>
</div>

<footer class="custom-footer">
<center>MYEG</center>
</footer>


<style>
    .custom-indent{
        margin-left: 20px;
    }
    body{
        background-color: #f5f8fa;
        color: #74787E;
    }
    .email-body{
        margin-left: 20px;
        margin-right: 20px; 
        background: white;
        padding: 20px;
    }
    .email-logo{
        height: 50px;
    }
    .custom-email-header{
        margin-left: 20px;
        margin-right: 20px;
        margin-top: 20px;
        padding: 10px;
        background: #c61e1f;
    }
    .custom-footer{
        background: #353535;
        padding: 20px;
        margin: 0 20px;
        margin-bottom: 20px;
    }
</style>