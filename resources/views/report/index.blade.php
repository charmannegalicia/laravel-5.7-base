@extends('layouts.dashboard.master')

@section('content')
    <div class="row" style="background: #ecf0f5;">
        <div class="col-md-12" >
                <div class="content-wrapper">
                    <section class="content-header">
                        <h1>
                            Reports
                        </h1>
                        <hr>
                        {{-- <ol class="breadcrumb">
                            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                            <li class="active">Dashboard</li>
                        </ol> --}}
                    </section>
                    <div class="col-md-12">
                            <div class="box box-primary">
                                <div class="box-header with-border">
                                  <h3 class="box-title">Transactions</h3>
                                </div>
                                {{-- <select name="" id="mySelect">
                                    <option value="Branch 1">Branch 1</option>
                                    <option value="Branch 2">Branch 2</option>
                                </select> --}}
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label>From:</label>
                        
                                        <div class="input-group date">
                                            <div class="input-group-addon">
                                                <i class="fa fa-calendar"></i>
                                            </div>
                                            <input type="text" class="form-control pull-right date-range-filter datePicker" data-date-format="mm/dd/yyyy" id="datePickerFrom">
                                        </div>
                                        <!-- /.input group -->
                                    </div>
                                </div>
                                <div class="col-md-2">
                                        <div class="form-group">
                                            <label>To:</label>
                            
                                            <div class="input-group date">
                                                <div class="input-group-addon">
                                                    <i class="fa fa-calendar"></i>
                                                </div>
                                                <input type="text" class="form-control pull-right date-range-filter datePicker" data-date-format="mm/dd/yyyy" id="datePickerTo">
                                            </div>
                                            <!-- /.input group -->
                                        </div>
                                </div>

                                {{-- Branch and Agent Dropdown Component --}}
                                <dropdown-filter-branch-agent :user="{{ auth()->user() }}" user-role="{{ auth()->user()->getRoleNames()->first() }}" :branches="{{ $branches }}" :users="{{ $users }}"></dropdown-filter-branch-agent>

                                <!-- /.box-header -->
                                <div class="box-body">
                                        <table class="table table-bordered" id="reports-table">
                                            <thead>
                                                <tr>
                                                    {{-- <th>Id</th> --}}
                                                    <th>Name</th>
                                                    <th>middlename</th>
                                                    <th>lastname</th>
                                                    <th>Branch</th>
                                                    <th>Email</th>
                                                    <th>Status</th>
                                                    <th>Created At</th>
                                                    <th>Updated At</th>
                                                    <th>Action</th>
                                                </tr>
                                            </thead>
                                        </table>
                                </div>
                                <!-- /.box-body -->
                            </div>
                        </div>
                    
                </div>
        </div>
    </div>
@endsection
@push('after-scripts')

<script>

    /* Custom filtering function which will search data in column four between two values */
    $.fn.dataTable.ext.search.push(
        function( settings, data, dataIndex ) {
            var min = new Date($('#datePickerFrom').val());
            var max = new Date($('#datePickerTo').val());
            var transactDate = moment(data[6]).startOf('day') || 0; // use data for the transactDate column
            console.log(transactDate)
            if ( ( isNaN( min ) && isNaN( max ) ) ||
                ( isNaN( min ) && transactDate <= max ) ||
                ( min <= transactDate   && isNaN( max ) ) ||
                ( min <= transactDate   && transactDate <= max ) )
            {
                return true;    
            }
            return false;
        }
    );

    $('#datePickerTo, #datePickerFrom').datepicker({
        autoclose: true,
        // timeFormat:  "hh:mm:ss"
    });

    $(document).ready(function() {
        var table = $('#reports-table').DataTable({
                
            // processing: true,
            // serverSide: true,
            ajax: '{!! route('dashboard.datatables.report') !!}',
            columns: [
                // { data: 'id', name: 'id',visible:false },
                { data: 'full_name', name: 'users.firstname'},
                { data: 'full_name', name: 'users.middlename', visible:false},
                { data: 'full_name', name: 'users.lastname', visible:false},
                { data: 'name', name: 'branches.name',orderable: false, searchable: true},
                { data: 'email', name: 'users.email' },
                { data: 'status', name: 'status', orderable: false, searchable: false },
                { data: 'created_at', name: 'users.created_at' },
                { data: 'updated_at', name: 'users.updated_at' },
                // { data: 'action', name: 'action', orderable: false, searchable: false },
                // { data: 'action', name: 'action', orderable: false, searchable: false}
            ],
        });
     
        // Event listener to the two range filtering inputs to redraw on input

        $('.datePicker').change( function() {
            table.draw();
        } );
        

        $('.search-agent').change( function() {
            table.search(this.value).draw();   
        } );

        $('.search-branch').change( function() {
            if(this.options[this.selectedIndex].text == 'All'){
                table.search('').draw();
            }else{
                table.search(this.options[this.selectedIndex].text).draw();   
            }
        } );
        
    
    } );
    
    
</script>
@endpush
    