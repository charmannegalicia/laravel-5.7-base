@extends('layouts.dashboard.master')

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="content-wrapper">
                <section class="content-header">
                    <h1>
                        User Management 
                        <small>Active Users</small>
                    </h1>
                    <p></p>
                    @if(session('success'))
                        <div class="callout callout-success">
                        <h4>{{ session('success')}}</h4>
                    @endif
                    {{-- <ol class="breadcrumb">
                        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                        <li class="active">Dashboard</li>
                    </ol> --}}
                </section>
                <div class="col-md-12">
                    <div class="box box-primary" style="padding: 20px;">
                        <div class="box-header with-border">
                            <h3 class="box-title"></h3>
                            <div class="box-tools pull-right">
                                <a href="{{ route('admin.users.create')}}">
                                    <button type="button" class="btn btn-success btn-xs" ><i class="fa fa-plus"></i> Add User
                                    </button>
                                </a>
                            </div>
                        </div>
                        <table class="table table-bordered" id="users-table">
                            <thead>
                                <tr>
                                    {{-- <th>Id</th> --}}
                                    <th>Name</th>
                                    <th>Email</th>
                                    <th>Status</th>
                                    <th>Created At</th>
                                    <th>Updated At</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <form action="{{ route('admin.users.destroy', 'destroy') }}" method="POST" id="formDelUser">
            @csrf
            @method('delete')
            <input type="hidden" class="user-id" name="user_id" value="">
        </form>
        {{-- <button type="button" class="btn btn-info btn-lg">Open Modal</button> --}}

        <!-- Modal -->
        <div id="myModal" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title"></h4>
            </div>
            <div class="modal-body">
                <p>Are you sure do you want to delete this user?</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary btn-submit-delete">Yes</button>
                <button type="button" class="btn btn-primary" data-dismiss="modal">No</button>
            </div>
            </div>

        </div>
        </div>
    </div>
@endsection
@push('after-scripts')
<script>
    $(document).ready(function(){
        $('#users-table').DataTable({
            processing: true,
            serverSide: true,
            ajax: '{!! route('admin.datatables.user') !!}',
            columns: [
                // { data: 'id', name: 'id' },
                { data: 'fullname', name: 'fullname' },
                { data: 'email', name: 'email' },
                { data: 'status', name: 'status', orderable: false, searchable: false },
                { data: 'created_at', name: 'created_at' },
                { data: 'updated_at', name: 'updated_at' },
                { data: 'action', name: 'action', orderable: false, searchable: false },
                // { data: 'action', name: 'action', orderable: false, searchable: false}
            ]
        });
    });

    // Delete User
    $(document).on('click', '.btn-delete-user',function(){
        $('.user-id').val($(this).attr('data-id'));
    }); 
    $(document).on('click', '.btn-submit-delete',function(){
        $('#formDelUser').submit();
    }); 

</script>
@endpush


