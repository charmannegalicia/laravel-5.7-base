@extends('layouts.dashboard.master')

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="content-wrapper">
                <section class="content-header">
                    <h1>
                        User Management
                    </h1>
                    <p></p>
                    {{-- <ol class="breadcrumb">
                        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                        <li class="active">Dashboard</li>
                    </ol> --}}
                </section>
                <div class="col-md-6">
                    <div class="box box-primary">
                        <div class="box-header with-border">
                            <h3 class="box-title">Edit User</h3>
                        </div>
                        <!-- /.box-header -->
                        <!-- form start -->
                        <form action="{{ route('admin.users.update', $user->id) }}" method="POST">
                            @csrf
                            @method('patch')
                            <div class="box-body">
                                <div class="form-group {{ $errors->has('firsname') ? 'has-error' : '' }}">
                                    <label for="firstname">First name*</label>
                                    <input type="text" class="form-control" id="firstname" name="firstname" value="{{ $user->firstname }}" required>
                                    @if ($errors->has('firstname'))
                                        <span class="help-block">{{ $errors->first('firstname') }}</span>
                                    @endif
                                </div>
                                <div class="form-group">
                                    <label for="middlename">Middle name</label>
                                    <input type="text" class="form-control" id="middlename" name="middlename" value="{{ $user->middlename  }}">
                                </div>
                                <div class="form-group {{ $errors->has('lastname') ? 'has-error' : '' }}">
                                    <label for="lastname">Last name*</label>
                                    <input type="text" class="form-control" id="lastname" name="lastname" value="{{ $user->lastname  }}" required>
                                    @if ($errors->has('lastname'))
                                        <span class="help-block">{{ $errors->first('lastname') }}</span>
                                    @endif
                                </div>
                                <div class="form-group {{ $errors->has('email') ? 'has-error' : '' }}">
                                    <label for="email">Email*</label>
                                    <input type="email" class="form-control" id="email" name="email" value="{{ $user->email }}" disabled>
                                    @if ($errors->has('email'))
                                        <span class="help-block">{{ $errors->first('email') }}</span>
                                    @endif
                                </div>
                                {{-- <div class="form-group {{ $errors->has('password') ? 'has-error' : '' }}">
                                    <label for="password">Password*</label>
                                    <input type="password" class="form-control" id="password" name="password" >
                                    @if ($errors->has('password'))
                                        <span class="help-block">{{ $errors->first('password') }}</span>
                                    @endif
                                </div>
                                <div class="form-group {{ $errors->has('password_confirmation') ? 'has-error' : '' }}">
                                    <label for="password_confirmation">Confirm Password*</label>
                                    <input type="password" class="form-control" id="password_confirmation" name="password_confirmation" >
                                </div> --}}
                                <div class="form-group {{ $errors->has('status') ? 'has-error' : '' }}">
                                    <label for="role">Status*</label>
                                    <select class="form-control" id="role" name="status" >
                                        <option style="display:none;">Select status</option>
                                        <option value="1" {{ (int)$user->is_active == 1 ? 'selected' : ''}}>Active</option>
                                        <option value="0" {{ (int)$user->is_active == 0 ? 'selected' : ''}}>Inactive</option>
                                    </select>
                                    @if ($errors->has('status'))
                                        <span class="help-block">{{ $errors->first('status') }}</span>
                                    @endif
                                </div>
                                <div class="form-group">
                                    <label for="role">Role*</label>
                                    <select class="form-control" id="role" name="role" required>
                                        <option style="display:none;">Select Role</option>
                                        @foreach($roles as $role)
                                            <option value="{{ $role->name }}" {{ $user->getRoleNames()->first() == $role->name ? 'selected' : '' }}>{{ ucwords($role->name) }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <!-- /.box-body -->
                    
                            <div class="box-footer">
                                <button type="submit" class="btn btn-primary">Submit</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('after-scripts')
<script>
</script>
@endpush
      
      
      