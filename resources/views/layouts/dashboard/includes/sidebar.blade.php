    <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- /.search form -->
        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu" data-widget="tree">
            <li class="header">General</li>
            <li  class="{{ Request::segment(1) == 'dashboard' && Request::segment(2) == null ?  'active' : '' }}">
                <a href="{{ route('admin.index') }}">
                    <i class="fa fa-dashboard"></i> <span>Dashboard</span>
                </a>
            </li>
            @hasanyrole('super admin')
            <li class="header">System</li>
            <li class="treeview {{ Request::segment(2) == 'users' ? 'active menu-open' : '' }}">
                <a href="#">
                    <i class="fa fa-users"></i> <span>Access Management</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li class="{{ Request::segment(2) == 'users' ?  'active' : '' }}"><a href="{{ route('admin.users.index') }}"><i class="fa fa-circle-o"></i>User Managment</a></li>
                    {{-- <li class=""><a href="#"><i class="fa fa-circle-o"></i>Roles Management</a></li> --}}
                </ul>
            </li>
            @endhasanyrole
    </ul>
    </section>
    <!-- /.sidebar -->
</aside>