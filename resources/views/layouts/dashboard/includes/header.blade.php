<header class="main-header">
    <!-- Logo -->
    <a href="#" class="logo">
        <!-- mini logo for sidebar mini 50x50 pixels -->
        <span class="logo-mini"><b>{{ (auth()->user()->firstname)[0]}}</b></span>
        <!-- logo for regular state and mobile devices -->
    <span class="logo-lg"><b>{{ ucwords(auth()->user()->getRoleNames()->first()) }}</b></span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top ">
        <!-- Sidebar toggle button-->
        <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
            <span class="sr-only">Toggle navigation</span>
        </a>

        <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
            <!-- User Account: style can be found in dropdown.less -->
            <li class="dropdown user user-menu">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        
                <span class="hidden-xs">{{ auth()->user()->lastname }}, {{ (auth()->user()->firstname)[0]}}</span>
                <i class="fa  fa-sort-down" style="padding-left: 10px;"></i>
                </a>
                <ul class="dropdown-menu">
                    <!-- User image -->
                    <li class="user-header">
                        {{-- <img src="dist/img/user2-160x160.jpg" class="img-circle" alt="User Image"> --}}

                        <p class="custom-username">
                        {{-- Alexander Pierce - Web Developer
                        <small>Member since Nov. 2012</small> --}}
                        {{ auth()->user()->firstname }} {{ auth()->user()->middlename }} {{ auth()->user()->lastname }}
                        </p>
                    </li>
                    <!-- Menu Footer-->
                    <li class="user-footer">
                        <div class="pull-left">
                        <a href="{{ route('admin.profile.edit', auth()->user()->id) }}" class="btn btn-default btn-flat">Profile</a>
                        </div>
                        <div class="pull-right">
                        <a href="{{ route('logout') }}" class="btn btn-default btn-flat" 
                            onclick="event.preventDefault();document.getElementById('logout-form').submit();">Sign out</a>
                        </div>
                        {{-- <a class="dropdown-item" href="{{ route('logout') }}"
                            onclick="event.preventDefault();
                                            document.getElementById('logout-form').submit();">
                            {{ __('Logout') }}
                        </a> --}}

                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            @csrf
                        </form>
                    </li>
                </ul>
                </a>
            </li>
        </ul>
      </div>
    </nav>
</header>