<div class="col-md-4 col-sm-6 col-xs-12">
    <div class="info-box">
        <span class="info-box-icon bg-aqua"><i class="fa fa-gears"></i></span>

        <div class="info-box-content">
            <span class="info-box-text">Transaction of the Day</span>
            <span class="info-box-number">105</span>
        </div>
      <!-- /.info-box-content -->
    </div>
    <!-- /.info-box -->
</div>

{{-- <div class="col-md-4 col-sm-6 col-xs-12">
    <div class="info-box">
        <span class="info-box-icon bg-yellow"><i class="fa fa-university"></i></span>

        <div class="info-box-content">
            <span class="info-box-text">Branches</span>
            <span class="info-box-number">{{ count($branches)}}</span>
        </div>
    </div>
</div> --}}

<div class="col-md-4 col-sm-6 col-xs-12">
    <div class="info-box">
        <span class="info-box-icon bg-green"><i class="fa fa-users"></i></span>

        <div class="info-box-content">
            <span class="info-box-text">Users</span>
            <span class="info-box-number">{{ count($users)}}</span>
        </div>
        <!-- /.info-box-content -->
    </div>
    <!-- /.info-box -->
</div>

{{-- <div class="col-md-12" style="background: #ecf0f5;">
    <div class="box box-primary">
        <div class="box-header with-border">
          <h3 class="box-title">Monthly Transaction</h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <quick-statistic></quick-statistic>
        </div>
        <!-- /.box-body -->
    </div>
</div> --}}