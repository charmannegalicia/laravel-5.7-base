@extends('layouts.dashboard.master')

@section('content')
    <div class="row">
        <div class="col-md-12">
                <div class="content-wrapper">
                    <section class="content-header">
                        <h1>
                            Dashboard
                        </h1>
                        <hr>
                        {{-- <ol class="breadcrumb">
                            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                            <li class="active">Dashboard</li>
                        </ol> --}}
                    </section>

                    {{-- @hasanyrole('super admin')
                        @include('dashboard.includes.admin.index')
                    @endhasanyrole

                    @hasanyrole('branch admin')
                        @include('dashboard.includes.branch-admin.index')
                    @endhasanyrole

                    @hasanyrole('agent')
                        @include('dashboard.includes.agent.index')
                    @endhasanyrole --}}
                    
                </div>
        </div>
    </div>
@endsection
@push('after-scripts')
<script>
</script>
@endpush


