@extends('layouts.dashboard.master')

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="content-wrapper">
                <section class="content-header">
                    <h1>
                        Edit Profile
                    </h1>
                    @if(session('success'))
                        <div class="callout callout-success">
                        <h4>{{ session('success')}}</h4>
                    @endif
                    <p></p>
                    {{-- <ol class="breadcrumb">
                        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                        <li class="active">Dashboard</li>
                    </ol> --}}
                </section>
                <div class="col-md-6">
                    <div class="box box-info">
                        <div class="box-header with-border">
                            {{-- <h3 class="box-title">Edit Profile</h3> --}}
                        </div>
                        <!-- /.box-header -->
                        <!-- form start -->
                        <form action="{{ route('admin.profile.update', $user->id) }}" method="POST">
                            @csrf
                            @method('patch')
                            <div class="box-body">
                                <div class="form-group {{ $errors->has('firsname') ? 'has-error' : '' }}">
                                    <label for="firstname">First name*</label>
                                    <input type="text" class="form-control" id="firstname" name="firstname" value="{{ $user->firstname }}" required>
                                    @if ($errors->has('firstname'))
                                        <span class="help-block">{{ $errors->first('firstname') }}</span>
                                    @endif
                                </div>
                                <div class="form-group">
                                    <label for="middlename">Middle name</label>
                                    <input type="text" class="form-control" id="middlename" name="middlename" value="{{ $user->middlename  }}" >
                                </div>
                                <div class="form-group {{ $errors->has('lastname') ? 'has-error' : '' }}">
                                    <label for="lastname">Last name*</label>
                                    <input type="text" class="form-control" id="lastname" name="lastname" value="{{ $user->lastname  }}" required>
                                    @if ($errors->has('lastname'))
                                        <span class="help-block">{{ $errors->first('lastname') }}</span>
                                    @endif
                                </div>
                                <div class="form-group {{ $errors->has('email') ? 'has-error' : '' }}">
                                    <label for="email">Email*</label>
                                    <input type="email" class="form-control" id="email" name="email" value="{{ $user->email }}" disabled>
                                    @if ($errors->has('email'))
                                        <span class="help-block">{{ $errors->first('email') }}</span>
                                    @endif
                                </div>
                                <div class="form-group {{ $errors->has('password') ? 'has-error' : '' }}">
                                    <label for="password">Password*</label>
                                    <input type="password" class="form-control" id="password" name="password" >
                                    @if ($errors->has('password'))
                                        <span class="help-block">{{ $errors->first('password') }}</span>
                                    @endif
                                </div>
                                <div class="form-group {{ $errors->has('password_confirmation') ? 'has-error' : '' }}">
                                    <label for="password_confirmation">Confirm Password*</label>
                                    <input type="password" class="form-control" id="password_confirmation" name="password_confirmation" >
                                </div>
                                @if(auth()->user()->getRoleNames()->first() != 'super admin')
                                <div class="form-group {{ $errors->has('branch_id') ? 'has-error' : '' }}">
                                    <label for="role">Branch*</label>
                                    <select class="form-control" id="role" name="branch_id" disabled>
                                        <option style="display:none;">Select Branch</option>
                                        @foreach ($branches as $branch)
                                            <option value="{{ $branch->id }}" {{ optional($user->branch)->id == $branch->id ? 'selected' : '' }}>{{ $branch->name }}</option>
                                        @endforeach
                                    </select>
                                    @if ($errors->has('branch_id'))
                                        <span class="help-block">{{ $errors->first('branch_id') }}</span>
                                    @endif
                                </div>
                                @endif
                            </div>
                            <!-- /.box-body -->
                    
                            <div class="box-footer">
                                <button type="submit" class="btn btn-primary">Submit</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('after-scripts')
<script>
</script>
@endpush
      
      
      