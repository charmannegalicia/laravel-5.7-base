{{-- @extends('errors::illustrated-layout')

@section('code', '419')
@section('title', __('Page Expired'))

@section('image')
    <div style="background-image: url({{ asset('/svg/403.svg') }});" class="absolute pin bg-cover bg-no-repeat md:bg-left lg:bg-center">
    </div>
@endsection

@section('message', __('Sorry, your session has expired. Please refresh and try again.')) --}}

@extends('layouts.app')

@section('content')
<div class="container">
        <center>
        <div class="login-container custom-border2">
            <div class="custom-header custom-border1">Error 419</div>
            <div class="custom-card">
                Sorry, your session has expired. Please refresh and try again.
                <br><br><br>
                <div class="form-group row">
                    <div class="col-md-12">
                        <button onclick="history.back(-1)" class="btn custom-button">
                            Go Back
                        </button>
                    </div>
                </div>
            </div>
        </div>
        </center>
</div>
@endsection
