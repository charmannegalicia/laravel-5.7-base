{{-- @extends('errors::illustrated-layout')

@section('code', '403')
@section('title', __('Forbidden'))

@section('image')
    <div style="background-image: url({{ asset('/svg/403.svg') }});" class="absolute pin bg-cover bg-no-repeat md:bg-left lg:bg-center">
    </div>
@endsection

@section('message', __($exception->getMessage() ?: 'Sorry, you are forbidden from accessing this page.')) --}}

@extends('layouts.app')

@section('content')
<div class="container">
        <center>
        <div class="login-container custom-border2">
            <div class="custom-header custom-border1">Error 403</div>
            <div class="custom-card">
                Sorry, you are forbidden from accessing this page.
                <br><br><br>
                <div class="form-group row">
                    <div class="col-md-12">
                        <button onclick="history.back(-1)" class="btn custom-button">
                            Go Back
                        </button>
                    </div>
                </div>
            </div>
        </div>
        </center>
</div>
@endsection