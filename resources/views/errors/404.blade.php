{{-- @extends('errors::illustrated-layout')

@section('code', '404')
@section('title', __('Page Not Found'))

@section('image')
    <div style="background-image: url({{ asset('/svg/404.svg') }});" class="absolute pin bg-cover bg-no-repeat md:bg-left lg:bg-center">
    </div>
@endsection

@section('message', __('Sorry, the page you are looking for could not be found.')) --}}

@extends('layouts.app')

@section('content')
<div class="container">
        <center>
        <div class="login-container custom-border2">
            <div class="custom-header custom-border1">Error 404</div>
            <div class="custom-card">
                Sorry, the page you are looking for could not be found.
                <br><br><br>
                <div class="form-group row">
                    <div class="col-md-12">
                        <button onclick="history.back(-1)" class="btn custom-button">
                            Go Back
                        </button>
                    </div>
                </div>
            </div>
        </center>
</div>
@endsection
