{{-- @extends('errors::illustrated-layout')

@section('code', '503')
@section('title', __('Service Unavailable'))

@section('image')
    <div style="background-image: url({{ asset('/svg/503.svg') }});" class="absolute pin bg-cover bg-no-repeat md:bg-left lg:bg-center">
    </div>
@endsection

@section('message', __($exception->getMessage() ?: 'Sorry, we are doing some maintenance. Please check back soon.'))
 --}}

@extends('layouts.app')

@section('content')
<div class="container">
        <center>
        <div class="login-container custom-border2">
            <div class="custom-header custom-border1">Error 503</div>
            <div class="custom-card">
                Sorry, we are doing some maintenance. Please check back soon.
                <br><br><br>
                <div class="form-group row">
                    <div class="col-md-12">
                        <button onclick="history.back(-1)" class="btn custom-button">
                            Go Back
                        </button>
                    </div>
                </div>
            </div>
        </div>
        </center>
</div>
@endsection