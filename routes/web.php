<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Main 
Route::get('/', 'HomeController@index')->name('index');


Route::get('/home', function () {
    return redirect()->route('admin.index');
});

// Defaul User Management
Route::get('/admin/dashboard', function () {
    // return view('layouts.admin.master');    
});

Auth::routes(['verify' => true]);

Route::patch('user/password/update', 'UserController@passwordUpdate')->name('user.password.update');
Route::get('user/password/change', 'UserController@changePassword')->name('user.password.change');

// Admin Dashboard 
Route::group(['prefix' => 'admin', 'as' => 'admin.', 'middleware' => ['verified']], function(){

    // Admin Dashboard
    Route::get('/dashboard', 'Admin\AdminController@index')->name('index');

    // DataTables
    Route::get('/dashboard/datatables/user', 'DatatablesController@userData')->name('datatables.user');

    // Individual Profile 
    Route::patch('/dashboard/profile/update/{user}', 'ProfileController@update')->name('profile.update');
    Route::get('/dashboard/profile/{user}/edit', 'ProfileController@edit')->name('profile.edit');

    // Users
    Route::resource('/dashboard/users', 'UserController');

});

Route::get('auth/{provider}', 'Auth\LoginController@redirectToProvider');
Route::get('auth/{provider}/callback', 'Auth\LoginController@handleProviderCallback');

