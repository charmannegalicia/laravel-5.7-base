<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class ResetPasswordEmailNotification extends Notification
{
    use Queueable;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public static $toMailCallback;

    public $token;

    public function __construct($token)
    {
        $this->token = $token;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
            ->subject('MYEG Change Password')
            ->from('no-reply@myeg.ph')
            ->line('
            <div style="margin-left: 20px;">
            Dear Valued Customer!<br><br>
            <div style="margin-left: 40px;">
                We’ve noticed that your personal details in MYEG Concierge Portal have been updated, or you may have forgotten your password.<br><br>
                We just want to make sure this is really you! Please use the link below to change your password and would only be valid up to sixty (60) minutes.<br><br>
                <a href="' . url('/') .''. route('password.reset', $this->token, false) . '">Reset Password my MYEG Concierge Account</a>
                <p></p>

            </div>
            </div>')
            ->line('
            <div style="margin-left: 20px;">
            <div style="margin-left: 40px;">
            <p>Thank you.</p>
            </div>
            <p><strong>Your MYEG Team</strong></p>
            </div>
            ');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }

    public static function toMailUsing($callback)
    {
        static::$toMailCallback = $callback;
    }
}
