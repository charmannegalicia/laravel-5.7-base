<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Carbon;

class VerifyEmailNotification extends Notification
{
    use Queueable;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        // if (static::$toMailCallback) {
        //     return call_user_func(static::$toMailCallback, $notifiable);
        // }

        return (new MailMessage)
            ->subject(Lang::getFromJson('MYEG Concierge Portal Successful Registration'))
            ->from('no-reply@myeg.ph')
            ->line('<div style="margin-left: 20px;">Dear Valued Customer,<br><br><div style="margin-left: 40px;">Welcome to MYEG Concierge Portal! Your account is now ready to be used. All you need to do is click the link below:</div></div>')
            ->line('<div style="margin-left:60px;"><br><a href="'.$this->verificationUrl($notifiable).'">Verify my MYEG Concierge account</a><p></p></div>')
            ->line('
            <div style="margin-left: 20px;">
            <div style="margin-left: 40px;">
            <p><strong>FEATURES</strong><br>
            <li>Save time and effort for having someone to run all the errands for your vehicle renewal.</li>
            <li>Efficiently purchase comprehensive insurance.</li>
            <li>Our concierge rider will pick-up the car from customer’s house and return the same with the renewal documents.</li>
            <li>Fully secured as our concierge staff are composed of professional drivers, and fully insured for any eventuality.</li>
            <li>Service is 100% FREE for those availing of Comprehensive Insurance from MYEG Insurance, while a Service Fee will be charged to those who opt not avail of the same.</li>
            <br>
            <p style="margin-bottom:0px;"><strong>Terms and Conditions </strong></p>
            <p style="font-size: 13px;">Read our full Terms and Conditions <a href="' . url('/'). '/terms-and-condition">here</a>.</p>
            <p>Thank you.</p>
            </div>
            <p><strong>Your MYEG Concierge Team</strong></p>
            </div>
            ');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }

    /**
     * Get the verification URL for the given notifiable.
     *
     * @param  mixed  $notifiable
     * @return string
     */
    protected function verificationUrl($notifiable)
    {
        return URL::temporarySignedRoute(
            'verification.verify', Carbon::now()->addMinutes(60), ['id' => $notifiable->getKey()]
        );
    }
}
