<?php

namespace App\Http\Controllers;

use App\User as Model;
use Spatie\Permission\Models\Role;
use Illuminate\Http\Request;

use Illuminate\Support\Str;

use Illuminate\Support\Facades\Mail;
use App\Mail\RegisterUser;
use Hash;

use App\Http\Requests\UserStoreRequest;

class UserController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        
        $this->middleware('role:super admin|branch admin', ['only' => ['index','create']]);

        $this->middleware('check.under.branches');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('user.index');
    }

    public function create()
    {
        $roles = Role::all();
        $userRole = auth()->user()->getRoleNames()->first();
        return view('user.create', compact('branches','roles','userRole'));
    }
    
    public function edit(Model $user)
    {
        $roles = Role::all();
        $userRole = auth()->user()->getRoleNames()->first();
        return view('user.edit', compact('user','branches','roles', 'userRole'));
    }

    public function update($id)
    {
        request()->validate([
            'firstname' => ['required', 'string', 'max:255'],
            'lastname' => ['required', 'string', 'max:255'],
        ]);

        $user = Model::find($id);
        $user->firstname     = request()->firstname;
        $user->middlename    = request()->middlename;
        $user->lastname      = request()->lastname;
        $user->is_active     = request()->status;
        // $user->password      = bcrypt(request()->password);
        $user->save();
        
        // Remove Role From User
        $user->removeRole($user->getRoleNames()->first());

        // Assign Role To User
        $this->assignUserRole($user, request()->role);

        return redirect()->route('admin.users.index')->with('success', 'User is successfully updated.');

    }

    public function store(UserStoreRequest $request)
    {
        $randomPass = Str::random();
        $user = Model::create([
            'firstname'     => $request->firstname,
            'middlename'    => $request->middlename,
            'lastname'      => $request->lastname,
            'email'         => $request->email,
            'is_active'     => '1',
            'password'      => bcrypt($randomPass),
        ]);

        // Send Mail for new registered user
        Mail::to($user->email)->send(new RegisterUser($user, $randomPass));

        // Assign Role
        $this->assignUserRole($user, $request->role);

        return redirect()->route('admin.users.index')->with('success', 'User is successfully created.');
    }

    public function destroy($status)
    {
        $user = Model::find(request()->user_id)->delete();

        return redirect()->route('admin.users.index')->with('success', 'User is successfully deleted.');
    }

    public function assignUserRole($user, $role)
    {
        $user->assignRole($role);
    }

    public function changePassword()
    {
        return view('auth.passwords.change-password');
    }

    public function passwordUpdate()
    {
        if (!Hash::check(request()->current_password, auth()->user()->password)) {
            return back()->with('invalid_current_pass', 'Current password is not existing.');
        }

        request()->validate([
            'password' => ['required', 'string', 'min:6', 'confirmed'],
            'current_password' => ['required'],
        ]);

        $user = Model::find(auth()->user()->id);
        $user->password = bcrypt(request()->password);
        $user->password_change_at = \Carbon\Carbon::now();
        $user->email_verified_at = \Carbon\Carbon::now();
        $user->save();

        return redirect()->route('admin.index')->with('success', 'Password is successfully updated.');
        
    }

}
