<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\User;

use Yajra\Datatables\Datatables;

class DataTablesController extends Controller
{
    /**
     * Process datatables ajax request.
     *
     * @return \Illuminate\Http\JsonResponse
     */

    // Users
    public function userData()
    {
        // If Super Admin
        if(auth()->user()->getRoleNames()->first() == 'super admin'){
            $users = User::select(['id','firstname', 'middlename', 'lastname', 'is_active', 'email', 'password', 'created_at', 'updated_at'])
                            ->where('id', '!=', auth()->user()->id);
        }

        return Datatables::of($users)
            ->addColumn('fullname', function ($user) {
                return "$user->firstname $user->middlename $user->lastname";
            })
            ->addColumn('status', function ($user) { 
                return $user->is_active ? 'Active' : 'Inactive';
            })
            ->addColumn('action', function ($user) {
                return '<a href="/admin/dashboard/users/'. $user->id .'/edit" class="btn btn-xs btn-primary"><i class="glyphicon glyphicon-edit"></i> Edit</a>
                        <button type="button" href=""  data-toggle="modal" data-target="#myModal" class="btn btn-xs btn-danger btn-delete-user" data-id="' . $user->id .'"><i class="glyphicon glyphicon-trash"></i> Delete</button>';
            })
            ->removeColumn('password')
            ->make(true);
    }


}