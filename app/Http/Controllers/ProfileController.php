<?php

namespace App\Http\Controllers;

use App\User;
use Spatie\Permission\Models\Role;
use Illuminate\Http\Request;

use App\Http\Requests\UserStoreRequest;

class ProfileController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function edit(User $user)
    {
        // Check if current information is editing
        if(auth()->user()->id != $user->id){
            return back();
        }

        return view('profile.edit', compact('user','branches'));
    }

    public function update($id)
    {
        request()->validate([
            'firstname' => ['required', 'string', 'max:255'],
            'lastname' => ['required', 'string', 'max:255'],
        ]);

        $user = User::find($id);
        $user->firstname     = request()->firstname;
        $user->middlename    = request()->middlename;
        $user->lastname      = request()->lastname;
        $user->password      = bcrypt(request()->password);
        $user->save();

        return back()->with('success', 'Successfully updated.');

    }
}
