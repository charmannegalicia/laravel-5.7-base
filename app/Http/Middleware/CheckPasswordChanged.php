<?php

namespace App\Http\Middleware;

use Closure;

class CheckPasswordChanged
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(auth()->user()->password_change_at == null){
            return redirect()->route('user.password.change');
        }
        
        return $next($request);
    }
}
