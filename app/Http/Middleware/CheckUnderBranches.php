<?php

namespace App\Http\Middleware;
use App\User;
use Closure;

class CheckUnderBranches
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        // If only under his/her branch
        if(auth()->user()->getRoleNames()->first() == 'branch admin'){
            if(optional($request->user)->branch_id != null){
                if($request->user->branch_id != auth()->user()->branch->id){
                    return back();
                }
            }
            
        }
        
        return $next($request);
    }
}
