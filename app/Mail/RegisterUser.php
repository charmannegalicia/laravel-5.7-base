<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class RegisterUser extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */

    protected $data;
    protected $password;
    
    public function __construct($user, $pass)
    {
        $this->data = $user;
        $this->password = $pass;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('emails.users.register')
                    ->from('no-reply@myeg.ph')
                    ->with([
                        'name' => $this->data->firstname,
                        'password' => $this->password,
                    ]);
    }
}
