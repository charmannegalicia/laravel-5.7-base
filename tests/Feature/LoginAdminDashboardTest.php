<?php

namespace Tests\Feature;

use Tests\TestCase;
use App\User;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class LoginAdminDashboardTest extends TestCase
{
    use RefreshDatabase;
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_if_redirected_to_login_if_accessing_dashboard()
    {
        $this->get('/dashboard')
            ->assertStatus(302)
            ->assertRedirect(route('login'));
    }

    public function test_if_authenticated_when_accessing_dashboard()
    {
        $user = factory(User::class)->make();

        $response = $this->actingAs($user)
            ->get('/login')
            ->assertRedirect(route('admin.index'));


    }

    public function test_user_with_correct_credentials()
    {
        $user = User::create([
            'firstname' => 'Eman',
            'lastname' => 'Mendiola',
            'email' => 'eman@eman.com1',
            'is_active' => '1',
            'password' => bcrypt($password = 'i-love-laravel'),
        ]);
        $response = $this->post('/login', [
            'email' => $user->email,
            'password' => $password,
        ]);
        $response->assertRedirect(route('admin.index'));

        

        $response = $this->assertAuthenticatedAs($user);

        $response = $this->actingAs($user);
        $response->get(route('admin.index'))->assertSee('Dashboard');
    }
}
